1. Clona este repositorio, que servirá como base para crear una aplicación de facturas con ayuda de los demás miembros 
de tu equipo: https://bitbucket.org/git-madrid-5/facturas-NOMBREEQUIPO/src (no hagas git clone directamente con esta URL)
2. Se te ha encargado empezar el esqueleto de la clase Factura. Crea una rama para ello a partir de la versión actualizada de develop.
3. El código fuente de la aplicación tiene que estar en la carpeta src/com/cursogit/facturas/
4. Crea un archivo llamado Factura.class para la clase Factura, con los siguientes miembros además del método main():
	public int num;
	public float base;
	public float tipoIva;
	public Estados estado;
5. Comenta la línea de la propiedad "estado", porque aún no está implementado el enum Estados.
6. Crea un commit con la clase.
7. Vuelca el trabajo a la rama develop y borra la rama que has usado.
8. Sube los cambios de develop al repositorio remoto.
9. Cuando tengas la interfaz y el enum (desarrollada por otro compañero) en tu repositorio local, crea una rama para aplicar ambas cosas en tu código. Comprueba además que te ha desaparecido
el archivo .keep, borrado por alguien del equipo.
10. Descomenta la línea que comentaste en el punto 5 y haz un commit con esta modificación.
11. Implementa la interfaz en tu clase Factura (no hace falta que implementes los métodos, 
déjalos en blanco).
12. Crea un commit con este último cambio.
13. Vuelca el trabajo a la rama develop y borra la rama que has usado.
14. Eres el encargado de preparar la primera versión que irá a producción: cuando tengas en tu repositorio local 
las tareas de tus compañeros de equipo, vuelca en master todos los commits que están en develop y no en master.
15. Añade una tag al último commit de master con la versión 0.1.0.
16. Sube los cambios de master al repositorio remoto, incluida la tag.